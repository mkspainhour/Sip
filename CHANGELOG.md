# Change Log


## Current

### v1.0.1
- Transition database hosts to MongoDB Atlas.
- NPM script functionality improvements.
- Code legibility tweaks.
- Readme tweaks.


## Previous

### v1.0.0
Original project completion.
