### Note: currently refactoring this codebase to reflect an improved understanding of maintainable web application architecture.

<br>

# [Sip](https://ms-sip-demo.glitch.me/) helps you manage your cocktail recipes.

- Save the cocktail recipe you told your friend you would try sometime.
- Pull up that cocktail recipe when you're finally in the mood for something experimental.
- Edit that recipe when you learn your friend is not great at making cocktails.
- Delete that recipe it when you learn that your friend was teasing you and didn't expect you to try to make it.

## Tools

HTML & CSS, JavaScript, jQuery, Node, Express, MongoDB & Mongoose, Mocha & Chai, Git, and NPM.

## API Structure

*The* 🔒 *icon indicates that the endpoint requires an authenticated session to interact with.*

### User Authorization

- POST /api/auth/sign-in
- GET /api/auth/sign-out

### Cocktails

- POST /api/cocktail/create 🔒
- PUT /api/cocktail/update 🔒
- DELETE /api/cocktail/delete 🔒
- GET /api/cocktail/:targetId

### Users

- POST /api/user/create
- GET /api/user/:username

## Screenshots

### Landing Page
<p align="center">
   <img src="readme resources/landing.png">
</p>

### Sign In
<p align="center">
   <img src="readme resources/signIn.png">
</p>

### User Dashboard
<p align="center">
   <img src="readme resources/userHome.png">
</p>

### Recipe Editor
<p align="center">
   <img src="readme resources/recipeEdit.png">
</p>

## Credit

Designed & Developed by Marshall Spainhour
